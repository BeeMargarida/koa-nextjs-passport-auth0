import next from 'next'
import type { Context } from 'koa'
import type { IncomingMessage, ServerResponse } from 'http'
import type { UrlWithParsedQuery } from 'url'
import type { NextServer } from 'next/dist/server/next'

export class NextApp {
    private readonly app: NextServer
    private readonly requestHandler: (
        req: IncomingMessage,
        res: ServerResponse,
        parsedUrl?: UrlWithParsedQuery | undefined
    ) => Promise<void>

    constructor (isDevelopment: boolean) {
        this.app = next({ dev: isDevelopment })
        this.requestHandler = this.app.getRequestHandler()
    }

    public async init () {
        await this.app.prepare()
    }

    public async render (ctx: Context): Promise<void> {
        await this.app.render(ctx.req, ctx.res, ctx.path, ctx.query)
        ctx.respond = false
    }

    public async renderError (ctx: Context): Promise<void> {
        const statusCode = parseInt(ctx.params.error_code, 10)

        ctx.res.statusCode = Number.isNaN(statusCode) ? 400 : statusCode
        await this.app.render(ctx.req, ctx.res, '/_error')
        ctx.respond = false
    }

    public async handleRequest (ctx: Context): Promise<void> {
        await this.requestHandler(ctx.req, ctx.res)
        ctx.respond = false
    }
}
