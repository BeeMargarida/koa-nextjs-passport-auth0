import type { Context } from "koa";
import passport from "koa-passport";
import querystring from "querystring";
import * as jwt from 'jsonwebtoken'
import { NextApp } from "./controllers";
import { Config, Server } from "./services";
import { ExtraVerificationParams, Profile, Strategy } from "passport-auth0";

export type UserData = {
  id: string;
  name: string;
  email: string;
  picture: string;
  refreshToken: string;
  jsonWebToken: string;
};

export type Auth0User = {
  id: string;
  displayName: string;
  picture: string;
  _json: {
    email: string;
  };
  refreshToken: string;
  jsonWebToken: string;
};

export type ExtraVerificationParamsWithJWT = ExtraVerificationParams & {
  // eslint-disable-next-line camelcase
  id_token: string;
};

// Helper functions
function isUserAuthenticated(ctx: Context) {
  if (!ctx.isAuthenticated()) return false;

  if (!ctx.state.user) return false;

  // TODO: refresh token logic

  return true;
}

async function main() {
  try {
    const server = new Server({
      sessionCookie: Config.get("SESSION_COOKIE_NAME") || "",
      secretKeys: [Config.get("AUTH0_SECRET_1"), Config.get("AUTH0_SECRET_2")],
      serverTimeout: Number(Config.get("DEFAULT_SERVER_TIMEOUT")),
      sessionTimeout: Number(Config.get("DEFAULT_SESSION_TIMEOUT")),
    });
    const nextApp = new NextApp(Config.get("NODE_ENV") === "development");
    await nextApp.init();

    // Setup passport configuration
    passport.use(
      "auth0",
      new Strategy(
        {
          domain: Config.get("AUTH0_DOMAIN"),
          clientID: Config.get("AUTH0_CLIENT_ID"),
          clientSecret: Config.get("AUTH0_CLIENT_SECRET"),
          callbackURL: `${Config.get("CLIENT_URL")}:${Config.get(
            "CLIENT_PORT"
          )}/callback`,
        },
        (
          accessToken: string,
          refreshToken: string,
          extraParams: ExtraVerificationParams,
          profile: Profile,
          done: (error: Error | null, user: Profile, jwtToken: string) => void
        ) => {
          console.log("[Verify] profile", profile);
          console.log("[Verify] extraParams", extraParams);
          
          // Verify user logic, i.e. check if user exists, if not create it

          // TODO: See if this is necessary
          const jsonWebToken = (extraParams as ExtraVerificationParamsWithJWT)
            .id_token;

          // TODO: Verify token signature?

          // These tokens will appear below in `authResponse`
          return done(null, profile, JSON.stringify({
            jsonWebToken: jsonWebToken,
            refreshToken: refreshToken,
            accessToken: accessToken
          }));
        }
      )
    );

    // Setup middleware in priority order
    server.addSessionMiddleware();
    server.addMiddleware([passport.initialize(), passport.session()]);

    passport.serializeUser((user, done) => {
      console.log("[Serialize] user", user);

      const adaptedUser = user as Auth0User;
      const userData: UserData = {
        id: adaptedUser.id,
        name: adaptedUser.displayName,
        picture: adaptedUser.picture,
        email: adaptedUser._json.email,
        refreshToken: adaptedUser.refreshToken,
        jsonWebToken: adaptedUser.jsonWebToken,
      };

      done(null, userData);
    });

    passport.deserializeUser((user: UserData, done) => {
      if (!user) {
        const error = new Error("deserializeUser error - missing userData");
        console.error(error);
        return done(error, null);
      }

      const deserializedUser = {
        email: user.email,
        name: user.name,
        picture: user.picture,
        refreshToken: user.refreshToken,
        accessToken: user.jsonWebToken,
      };
      done(null, deserializedUser);
    });

    // Authentication routes
    server.get("/login", [
      (ctx: Context, next: () => Promise<void>) => {
        return passport.authenticate("auth0", {
          scope: "openid email profile offline_access",
          prompt: "login",
        })(ctx, next);
      },
    ]);

    server.get("/callback", [
      (ctx: Context, next: () => Promise<void>) => {
        return passport.authenticate(
          "auth0",
          async (err: unknown, user: Auth0User, authResponse) => {
            console.log("[callback] session", ctx.session);
            console.log("[callback] user", user);

            if (err || !user) {
              console.warn(
                `logging out due to bad auth response ${JSON.stringify(
                  authResponse
                )} ${err}`
              );

              // Back to the login
              return ctx.redirect("/login");
            }

            try {
              const { jsonWebToken, refreshToken, accessToken } = JSON.parse(authResponse);

              const decodedJwt = jwt.decode(jsonWebToken, { complete: true })

              console.log("[callback]", decodedJwt);

              await ctx.logIn({ ...user, jsonWebToken, refreshToken, accessToken });

              // Redirect to the intended path prior
              // to the authentication
              if (ctx.session && ctx.session.redirectRoute) {
                const redirectRoute = ctx.session.redirectRoute;
                ctx.session.redirectRoute = undefined;

                return ctx.redirect(redirectRoute);
              }

              return ctx.redirect("/");
            } catch (error) {
              next();
            }
          }
        )(ctx, next);
      },
    ]);

    server.get("/logout", [
      (ctx: Context, next: () => Promise<void>) => {
        ctx.logout();

        // passport-auth0 does not handle logout, so this is necessary,
        // yoinked from harvest
        const logoutRedirectPath = `${Config.get("CLIENT_URL")}:${Config.get(
          "CLIENT_PORT"
        )}/login`;
        const searchString = querystring.stringify({
          client_id: Config.get("AUTH0_CLIENT_ID"),
          returnTo: logoutRedirectPath,
        });

        const logoutURL = new URL(`https://${Config.get("AUTH0_DOMAIN")}/v2/logout`);
        logoutURL.search = searchString;

        console.log("[logout], logoutURL", logoutURL);

        return ctx.redirect(logoutURL.toString());
      },
    ]);

    // Route to error page for unauthorized access
    server.get("/_error", [[nextApp, nextApp.renderError]]);

    // Public route
    server.get("/about", [[nextApp, nextApp.handleRequest]]);

    // Protected routes
    server.all(
      ["/", "/internal"],
      [
        (ctx: Context, next: () => Promise<void>) => {
          console.log("[internal] isAuthenticated", ctx.isAuthenticated());
          if (!isUserAuthenticated(ctx)) {
            // Save redirect path before redirecting
            if (ctx.session) {
              ctx.session.redirectRoute = ctx.originalUrl;
              ctx.session.save();
            }
            return ctx.redirect("/login");
          }

          console.log("[internal] ctx state", ctx.state);

          return next();
        },
        [nextApp, nextApp.render],
      ]
    );

    // Serve all other assets and routes
    server.get('(.*)', [[nextApp, nextApp.handleRequest]])

    server.listen(Number(Config.get("SERVER_PORT")));

    console.info(
      `Server runnning at ${Config.get("SERVER_URL")}:${Config.get(
        "SERVER_PORT"
      )}`
    );
    console.info(
      `Visit App at ${Config.get("CLIENT_URL")}:${Config.get("CLIENT_PORT")}`
    );
  } catch (err) {
    const error = err as Error;

    console.error(error);
    process.exit(1);
  }
}

export default main();
