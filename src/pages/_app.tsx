import '@/styles/globals.css'
import { IncomingMessage } from 'http'
import type { AppContext, AppProps } from 'next/app'

interface IncomingMessageWithUser extends IncomingMessage {
  user: Record<string, unknown>
}

type AdditionalProps = {
  user?: Record<string, unknown>
}

type PageProps = AppContext & AdditionalProps | AdditionalProps

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

App.getInitialProps = async ({ Component, ctx }: AppContext) => {
  const { req } = ctx
  

  const pageProps: PageProps = Component.getInitialProps
        ? await Component.getInitialProps(ctx) ?? {}
        : {}

  if (!req) return { pageProps };

  pageProps.user = (req as IncomingMessageWithUser).user;

  console.log("PROPS user", pageProps.user);

  return { pageProps }
};