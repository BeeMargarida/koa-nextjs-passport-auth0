import { ErrorRoute, InternalRoute, PageRoute, PublicRoute } from "./services"

/*
* These Routes are public and not protected by any auth
*/
export const PublicRoutes = {
    Home: new PublicRoute('/'),
    Login: new PublicRoute('/login'),
    LoginCallback: new PublicRoute('/callback'),
    Logout: new PublicRoute('/logout'),
    All: new PublicRoute('(.*)'),
}

export const InternalRoutes = {
    AllAuth0: new InternalRoute('/auth0/:splat*')
}

/*
* These Routes are public and not protected by any auth
*/
export const ErrorRoutes = {
    NextError: new ErrorRoute('/_error'),
    ErrorIndex: new ErrorRoute('/error/[error-code]'),
    Unauthorised: new ErrorRoute('/error/401'),
    NotAvailable: new ErrorRoute('/error/503'),
    InternalServerError: new ErrorRoute('/error/500'),
    NotFound: new ErrorRoute('/error/404'),
    Forbidden: new ErrorRoute('/error/403'),
    InvalidRequest: new ErrorRoute('/error/400')
}

/*
* These routes are protected by authentication
* They can be accessed by all user types
*/
export const PageRoutes = {
    Index: new PageRoute('/'),
    Internal: new PageRoute('/internal')
}
