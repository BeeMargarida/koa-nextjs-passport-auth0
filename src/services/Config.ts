import * as dotenv from 'dotenv'
import getConfig from 'next/config'

interface IConfig {
    init: () => void
    get: (key: any, defaultValue?: string) => string
}

dotenv.config()

class ConfigClass implements IConfig {
    private isInitialized = false
    private data = {
        SERVER_URL: 'http://localhost',
        SERVER_PORT: '3000',
        CLIENT_URL: 'http://localhost',
        CLIENT_PORT: '3000',
        AUTH0_DOMAIN: '',
        AUTH0_CLIENT_ID: '',
        AUTH0_CLIENT_SECRET: '',
        AUTH0_SECRET_1: 'harvest secret key',
        AUTH0_SECRET_2: 'another secret key',
        SESSION_COOKIE_NAME: 'experiment:sess',
        JWT_COOKIE_NAME: 'experiment_jwt',
        DEFAULT_SERVER_TIMEOUT: '120000',
        DEFAULT_SESSION_TIMEOUT: '1209600000', /** 14 days in milliseconds */
        DEFAULT_FETCH_TIMEOUT: '60000',
        NODE_ENV: 'development'
    }

    public init () {
        const nextConfig = getConfig()

        for (const key of Object.keys(this.data)) {
            const configKey = key as keyof ConfigClass['data']
            const overridenValue = nextConfig?.publicRuntimeConfig?.[configKey] || process.env[configKey]

            if (overridenValue) {
                this.data[configKey] = overridenValue
            }
        }

        this.isInitialized = true
    }

    public get (key: keyof ConfigClass['data'], defaultValue?: string): string {
        if (!this.isInitialized) {
            this.init()
        }

        // If provided a different default value
        // disregard the Config class's defaults
        if (defaultValue) {
            return process.env[key] ?? defaultValue
        }

        return this.data[key]
    }
}

export const Config = new ConfigClass()
Config.init()
